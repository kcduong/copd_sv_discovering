#!/usr/bin/python3
"""
Small Python script just to determine what the QUALITY column in a VCF file consists of
"""
import argparse
import sys

import vcf


def read_file(infile, outfile):
    x = set()
    vcf_reader = vcf.Reader(open(infile, 'r'), strict_whitespace=True)
    for rec in vcf_reader:
        x.add(rec.QUAL)

    output_file = "output/{}_quality.csv".format(outfile)
    with open(output_file, "w+") as output:
        for item in x:
            output.write("{}\n".format(item))


def main(argv):
    print(' '.join(argv))

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v', '--vcf', action='store', dest='vcf',
                        help="Specify the .vcf file containing structural variant calls", required=True)
    parser.add_argument('-o', '--output', action='store', dest='output', default="dummy",
                        help="Provide an output name, i.e. 'output/<NAME>_genes.csv'")
    args = parser.parse_args()
    read_file(args.vcf, args.output)


if __name__ == '__main__':
    main(sys.argv)