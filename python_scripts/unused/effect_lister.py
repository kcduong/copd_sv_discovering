#!/usr/bin/python3
"""
Small script to list all 'EFFECT's of the structural variants on genes, taken from Annovar annotation.
"""
import argparse
import sys

import vcf


def read_file(infile, outfile):
    """extracts all possible EFFECTs of SVs on the genes"""
    x = set()
    vcf_reader = vcf.Reader(open(infile, 'r'), strict_whitespace=True)
    for rec in vcf_reader:
        if str(rec.ALT[0]) == "<DEL>":
            if len(rec.INFO['ANN'][0].split('|')[1].split('&')) > 1:
                for effect in rec.INFO['ANN'][0].split('|')[1].split('&'):
                    x.add(effect)
            else:
                x.add(rec.INFO['ANN'][0].split("|")[1])

    output_file = "output/{}_effects.csv".format(outfile)
    with open(output_file, "w+") as output:
        for item in x:
            output.write("{}\n".format(item))


def main(argv):
    print(' '.join(argv))

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v', '--vcf', action='store', dest='vcf',
                        help="Specify the .vcf file containing structural variant calls", required=True)
    parser.add_argument('-o', '--output', action='store', dest='output', default="dummy",
                        help="Provide an output name, i.e. 'output/<NAME>_effects.csv'")
    args = parser.parse_args()
    read_file(args.vcf, args.output)


if __name__ == '__main__':
    main(sys.argv)