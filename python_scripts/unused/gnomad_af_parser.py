#!/usr/bin/python3
"""
Python script that finds gnomAD SV data for every SV call in the DELLY input file.
"""

import argparse
import datetime
import os
import sys

import pandas as pd
import vcf


def get_gnomad_af(infile, gnomad, outfile, sift):
    """Extracts gnomAD data for each SV call in the VCF file"""
    rows_list = []
    with open(infile, 'r') as vcf_input:

        vcf_reader = vcf.Reader(vcf_input, strict_whitespace=True)

        for rec in vcf_reader:

            svtype = rec.INFO['SVTYPE']
            if rec.INFO['SVTYPE'] == 'BND':
                continue
            chrom = rec.CHROM.split('chr', 1)[1]
            min_pos = rec.POS - 10000
            max_pos = rec.POS + 10000

            sv_len = abs(rec.INFO['END'] - rec.POS)

            sift_dir = os.path.join(sift)
            sift_dict = {'chrom': chrom, 'svtype': svtype, 'minpos': min_pos, 'maxpos': max_pos, 'sift': sift_dir,
                         'gnomad': gnomad, 'outfile': outfile}
            command = 'java -jar {sift}SnpSift.jar filter "( CHROM = \'{chrom}\') & ( SVTYPE = \'{svtype}\') & ' \
                      '( POS > {minpos}) & ( POS < {maxpos})" {gnomad} > output/{outfile}tmp.vcf'.format(**sift_dict)
            stream = os.popen(command).read()

            with open('output/{}tmp.vcf'.format(outfile), 'r') as tmp_records:
                if any(not line.startswith("#") for line in tmp_records):
                    close_pos = 99999
                    tmp_records.seek(0)
                    tmp_reader = vcf.Reader(tmp_records, strict_whitespace=True)
                    for m in tmp_reader:
                        if abs(abs(int(m.INFO['SVLEN'][0])) - sv_len) < close_pos:
                            close_pos = abs(abs(int(m.INFO['SVLEN'][0])) - sv_len)
                            match = m
                else:
                    continue

            dict1 = {}
            dict1.update(ID=rec.ID, GD_ID=match.ID, GD_AF=match.INFO['AF'][0], EUR_AN=match.INFO['EUR_AN'][0],
                         EUR_AF=match.INFO['EUR_AF'][0])

            rows_list.append(dict1)
    df = pd.DataFrame(rows_list)
    output_file = "output/{}_AF.csv".format(outfile)
    with open(output_file, "w+") as output:
        df.to_csv(output, sep=',', mode='w', line_terminator='\n', index=False)


def main(argv):
    print(' '.join(argv))
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v', '--vcf', action='store', dest='vcf',
                        help="Specify the .vcf file containing structural variant calls", required=True)
    parser.add_argument('-g', '--gnomad', action='store', dest='gnomad',
                        help="Specify the .vcf file containing gnomAD's structural variant calls", required=True)
    parser.add_argument('-s', '--sift', action='store', dest='sift',
                        help="Specify the directory that contains the SnpSift.jar executable, e.g. '~/Documents/snpEff",
                        required=True)
    parser.add_argument('-o', '--output', action='store', dest='output', default="dummy",
                        help="Provide an output name, i.e. 'output/<NAME>_AF.csv'")
    args = parser.parse_args()
    print("Started at: " + datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))
    get_gnomad_af(args.vcf, args.gnomad, args.output, args.sift)
    print("Finished at: " + datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S"))

    
if __name__ == '__main__':
    main(sys.argv)
