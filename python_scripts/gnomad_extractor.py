#!/usr/bin/python3
"""
Extracts gnomAD relevant columns from AnnotSV output file and stores it in a CSV file.
"""

import argparse
import csv
import sys

import pandas as pd


maxInt = sys.maxsize
while True:
    try:
        csv.field_size_limit(maxInt)
        break
    except OverflowError:
        maxInt = int(maxInt/10)


def read_file(tsv, output):
    """Reads in AnnotSV's TSV file and stores extracted data in a CSV table file"""
    rows_list = []
    with open(tsv) as input_file:
        reader = csv.DictReader(input_file, delimiter='\t', quoting=csv.QUOTE_NONE)

        for row in reader:
            dicto = {}
            orig_id = row['ID']
            gnom_id = row['GD_ID']
            gnom_an = row['GD_AN']
            gnom_af = row['GD_AF']
            gnom_popmaxaf = row['GD_POPMAX_AF']
            gnom_id_others = row['GD_ID_others']
            orig_svlength = row['SV length']

            dicto.update(ID=orig_id, SV_length=orig_svlength, GD_ID=gnom_id, GD_AN=gnom_an, GD_AF=gnom_af,
                         GD_POPMAX_AF=gnom_popmaxaf, GD_ID_others=gnom_id_others)

            rows_list.append(dicto)
    df = pd.DataFrame(rows_list)
    output_file = "output/gnomad_{}.csv".format(output)
    with open(output_file, 'w') as outfile:
        df.to_csv(outfile, sep=',', mode='w', line_terminator='\n', index=False)


def main(argv):
    print(' '.join(argv))

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-t', '--tsv', action='store', dest='tsv',
                        help="Specify the .tsv file containing the AnnotSV output", required=True)
    parser.add_argument('-o', '--output', action='store', dest='output', default="dummy",
                        help="Provide an output name, i.e. 'output/gnomad_<NAME>.csv'")
    args = parser.parse_args()
    read_file(args.tsv, args.output)


if __name__ == '__main__':
    main(sys.argv)