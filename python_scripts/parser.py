#!/usr/bin/python3
"""
A small Python script that parses through every SV call of a VCF file to extract values of interest for follow-up
research. Small calculations, like calculating the allele frequency are included. Extracted data gets stored in a
CSV file.
"""

import argparse
import sys

import pandas as pd
import vcf

chr_list = ['chr1', 'chr2', 'chr3', 'chr4', 'chr5', 'chr6', 'chr7', 'chr8', 'chr9', 'chr10', 'chr11', 'chr12', 'chr13',
            'chr14', 'chr15', 'chr16', 'chr17', 'chr18', 'chr19', 'chr20', 'chr21', 'chr22', 'chrX', 'chrY', 'chrM']


def count_samples(samples):
    """Counts number of COPD and control samples with the SV allele for each SV call"""
    case_counter = 0
    control_counter = 0
    mutant_cases = []
    for sample in samples:
        if sample.data.GT == './.' or sample.data.GT == '0/0':
            continue
        mutant_cases.append(sample.sample)
        if sample.sample.startswith("S"):
            control_counter += 1
        else:
            case_counter += 1
    mutant_samples = ';'.join(mutant_cases)

    return case_counter, control_counter, mutant_samples


def get_af(samples):
    """Calculates allele frequency by dividing amount of mutant alleles by the total known alleles"""
    allele_number = 0
    mutant_count = 0
    for sample in samples:
        if sample.data.GT != './.':
            if sample.data.GT == '0/0':
                allele_number += 2
            elif sample.data.GT == '1/0' or sample.data.GT == '0/1':
                allele_number += 2
                mutant_count += 1
            elif sample.data.GT == '1/1':
                allele_number += 2
                mutant_count += 2
    if allele_number == 0 or mutant_count == 0:
        allele_frequency = 0
    else:
        allele_frequency = mutant_count/allele_number

    return allele_frequency


def read_file(infile, outfile):
    """Reads in the VCF file in order to extract SV call data and store it in a CSV file"""
    rows_list = []
    case_total = 0
    control_total = 0
    with open(infile, 'r') as vcf_input:
        vcf_reader = vcf.Reader(vcf_input, strict_whitespace=True)
        for sample in vcf_reader.samples:
            if sample.startswith("S"):
                control_total += 1
            else:
                case_total += 1
        for rec in vcf_reader:

            dict1 = {}
            gene = rec.INFO['ANN'][0].split("|")[3]
            alt = rec.INFO['SVTYPE']

            if alt == "BND":
                end_chromosome = rec.INFO['CHR2']
                end_coordinate = rec.INFO['END']
                end_position = end_chromosome + ";" + str(end_coordinate)
                sv_length = ""
            else:
                end_position = rec.INFO['END']
                sv_length = end_position - rec.POS

            if "PRECISE" in rec.INFO:
                precise = 1
            else:
                precise = 0

            if gene == "":
                gene = rec.INFO['ANN'][1].split("|")[3]

            if "&" in gene:
                if "LINC00486" in gene.split("&")[0]:
                    gene = gene.split("&")[1]
                else:
                    gene = gene.split("&")[0]

            cases, controls, mutants = count_samples(rec.samples)
            if cases == 0:
                continue
            if cases == case_total and controls == control_total:
                continue

            allele_frequency = get_af(rec.samples)

            dict1.update(CHROM=rec.CHROM, START=rec.POS, ID=rec.ID, ALT=alt, GENE=gene, cases=cases, controls=controls,
                         AF=allele_frequency, Samples_ID=mutants, END=end_position, LENGTH=sv_length,
                         PRECISE=precise)

            rows_list.append(dict1)

        df = pd.DataFrame(rows_list)
        output_file = "output/{}_genes.csv".format(outfile)
        with open(output_file, "w+") as output:
            df.to_csv(output, sep=',', mode='w', line_terminator='\n', index=False)
        sample_list_file = "output/{}_samples.txt".format(outfile)
        with open(sample_list_file, "w+") as sample_output:
            for sample in vcf_reader.samples:
                sample_output.write("%s\n" % sample)


def main(argv):
    print(' '.join(argv))

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v', '--vcf', action='store', dest='vcf',
                        help="Specify the .vcf file containing structural variant calls", required=True)
    parser.add_argument('-o', '--output', action='store', dest='output', default="dummy",
                        help="Provide an output name, i.e. 'output/<NAME>_genes.csv'")
    args = parser.parse_args()
    read_file(args.vcf, args.output)


if __name__ == '__main__':
    main(sys.argv)
