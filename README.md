# Analysing Structural Variants in Exome Sequencing Data of SEO COPD Patients

A collection of small Python and R scripts used to extract and analyse structural variant data.  
The main analysis steps are done using other tools mentioned in the report. 
These scripts are to extract the data afterwards and create figures and tables.


## Getting Started

This repository consists of Python and R scripts. these can be found in their respective subdirectories.  
These scripts are run separately, although some will require the output file from another script for its input.

**Python**

* **parser.py** -  Parses through every SV call of a VCF file to extract variables and stores them into a CSV table.  
    * Requires VCF file containing annotated ([snpSift](https://pcingola.github.io/SnpEff/)) structural variants.   
* **gnomad_extractor.py** - Extracts gnomAD relevant columns from AnnotSV output file and stores them into a CSV table.  
    * Requires a TSV file generated by [AnnotSV](https://www.lbgi.fr/AnnotSV/) that contains gnomAD columns
    
**R**

* **gene_description.R** - Adds gene description to genes.  
    * Requires the CSV table output from parser.py.  
* **chisquare.R** - Performs the Chi-square test on structural variant data.  
    * Requires CSV table output from gene_description.R.  
* **gnomad.R** - Creates plots for allele frequencies and its distribution.  
    * Requires the merged full CSV table output from chisquare.R.  
* **preburden.R** - Converts previous table data into data files suitable for burden analysis.  
    * Requires the merged full CSV table output from chisquare.R.  
    * Requires a TXT file with list of sample IDs.  
* **burden.R** - Performs burden analysis.  
    * Requires all 4 output files from preburden.R

## Built With
Python version 3.7  
R version 4.0.2

And the following external libraries/packages:
```
| Library/Package 	| Version 	|
|-----------------	|---------	|
| Pandas          	| 0.25.3  	|
| PyVCF           	| 0.6.8   	|
| biomaRt         	| 2.44.1  	|
| SKAT            	| 2.0.0   	|
```

## Author
* **Kim Chau Duong** - [kcduong](https://bitbucket.org/kcduong/)
